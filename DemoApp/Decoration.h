//
//  Decoration.h
//  DemoApp
//
//  Created by yeosblue on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Decoration : UIImageView <UIGestureRecognizerDelegate>{
    UIPinchGestureRecognizer *pinch;
    UIRotationGestureRecognizer *rotate;
    UIPanGestureRecognizer *pan;
}

@end
