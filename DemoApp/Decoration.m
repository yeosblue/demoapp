//
//  Decoration.m
//  DemoApp
//
//  Created by yeosblue on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Decoration.h"

@implementation Decoration

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFired)];
        [tap setNumberOfTapsRequired:1];
        [tap setNumberOfTouchesRequired:1];
        [tap setDelegate:self];
        [self addGestureRecognizer:tap];
        
        pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panFired)];
        [pan setDelegate:self];
        [self addGestureRecognizer:pan];
        
        pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchFired)];
        [pinch setDelegate:self];
        [self addGestureRecognizer:pinch];
        
        rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotateFired)];
        [rotate setDelegate:self];
        [self addGestureRecognizer:rotate];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)tapFired
{
    NSLog(@"tap fired");
}

- (void)pinchFired
{
    NSLog(@"pinch fired -> scale: %.2f, velocity: %.2f", [pinch scale], [pinch velocity]);
    [self setTransform:CGAffineTransformScale(self.transform, pinch.scale, pinch.scale)];
    [pinch setScale:1];    
}

- (void)rotateFired
{
    NSLog(@"rotate fired -> rotate: %.2f", [rotate rotation]);
    [self setTransform:CGAffineTransformRotate(self.transform, rotate.rotation)];
    [rotate setRotation:0];
}

- (void)panFired
{
    NSLog(@"pan fired");
    CGPoint newCenter = [pan locationInView:[self superview]];
    [self setCenter:newCenter];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end