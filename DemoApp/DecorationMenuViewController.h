//
//  DecorationMenuViewController.h
//  DemoApp
//
//  Created by yeosblue on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DecorationDelegate <NSObject>

- (void)addDiamondDeco;

@end

@interface DecorationMenuViewController : UIViewController
@property (strong, nonatomic) id delegate;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *diamondDeco;

@end
