//
//  DecorationMenuViewController.m
//  DemoApp
//
//  Created by yeosblue on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DecorationMenuViewController.h"

@interface DecorationMenuViewController ()

@end

@implementation DecorationMenuViewController
@synthesize delegate;
@synthesize scrollView;
@synthesize diamondDeco;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(diamondTapped)];
    [tap setNumberOfTapsRequired:1];
    [diamondDeco addGestureRecognizer:tap];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setDiamondDeco:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)diamondTapped
{
    NSLog(@"diamond tapped");
    if (delegate && [delegate canPerformAction:@selector(addDiamondDeco) withSender:self]) {
        [delegate addDiamondDeco];
    }
}

@end
