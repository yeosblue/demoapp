//
//  Polaroid.h
//  DemoApp
//
//  Created by yeosblue on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Decoration;

@interface Polaroid : UIView 
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) NSMutableArray *decorations;

- (void)addDecoration:(Decoration *)decoration;
@end
