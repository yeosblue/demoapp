//
//  Polaroid.m
//  DemoApp
//
//  Created by yeosblue on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Polaroid.h"
#import <QuartzCore/QuartzCore.h>
#import "Decoration.h"

@implementation Polaroid
@synthesize imageView;
@synthesize decorations;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor whiteColor]];
        decorations = [NSMutableArray array];
        
        // Content image view
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 630, 800)];
        [imageView setCenter:self.center];
        NSLog(@"image view frame after setCenter: %@", NSStringFromCGRect(imageView.frame));
        [self addSubview:imageView];
        
        // Shadow
        [self.layer setMasksToBounds:NO];
        [self.layer setShadowOffset:CGSizeMake(0, 10)];
        [self.layer setShadowOpacity:0.5];
        [self.layer setShadowRadius:6];
    }
    return self;
}

- (id)init {
    return [self initWithFrame:CGRectMake(0, 0, 700, 860)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (void)addDecoration:(Decoration *)decoration
{
    [self addSubview:decoration];
    [decorations addObject:decoration];
}

@end
