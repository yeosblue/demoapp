//
//  ViewController.h
//  DemoApp
//
//  Created by yeosblue on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DecorationMenuViewController.h"

@class MyView;
@class Polaroid;
@class DecorationMenuViewController;

@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, DecorationDelegate>{
    MyView *myView;
    Polaroid *myPolaroid;
    UIImagePickerController *imagePicker;
    UIPopoverController *popover;
    DecorationMenuViewController *decoMenu;
    UIPopoverController *decorationPopover;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *photoBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *decoBarButton;

- (IBAction)openPhotos:(id)sender;
- (IBAction)addDecoration:(id)sender;
- (IBAction)saveImage:(id)sender;

@end
