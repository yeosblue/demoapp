//
//  ViewController.m
//  DemoApp
//
//  Created by yeosblue on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "Polaroid.h"
#import "DecorationMenuViewController.h"
#import "Decoration.h"
#import "MBProgressHUD.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize photoBarButton;
@synthesize decoBarButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup ater loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"fabric.jpg"]]];
    
    // Prepare image holder
    myPolaroid = [[Polaroid alloc] init];
    CGPoint center;
    center.x = self.view.center.x;
    center.y = self.view.center.y - 40;
    [myPolaroid setCenter:center]; 
    [myPolaroid setHidden:YES];
    [self.view addSubview:myPolaroid];
    
    // Prepare image picker
    // UIImagePickerController in iPad should be wrapped in UIPopoverController
    imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setAllowsEditing:YES];
    [imagePicker setDelegate:self];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
    
    // Prepare decoration menu
    decoMenu = [[self storyboard] instantiateViewControllerWithIdentifier:@"decorationMenu"];
    [decoMenu setDelegate:self];
    decorationPopover = [[UIPopoverController alloc] initWithContentViewController:decoMenu];
    
    if ([myPolaroid isHidden]) {
        [decoBarButton setEnabled:NO];
    }
}

- (void)viewDidUnload
{
    [self setPhotoBarButton:nil];
    [self setDecoBarButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (IBAction)openPhotos:(id)sender {
    if ([popover isPopoverVisible]) {
        [popover dismissPopoverAnimated:YES];
    } else {
        [popover presentPopoverFromBarButtonItem:photoBarButton permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
}

- (IBAction)addDecoration:(id)sender {
    if ([decorationPopover isPopoverVisible]) {
        [decorationPopover dismissPopoverAnimated:YES];
    } else {
        [decorationPopover presentPopoverFromBarButtonItem:decoBarButton permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
}

- (IBAction)saveImage:(id)sender {
    
    MBProgressHUD *mph = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    mph.labelText = @" Saving, wait for a while  ";
    
    UIGraphicsBeginImageContext(myPolaroid.imageView.frame.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0, 0, myPolaroid.imageView.frame.size.width, myPolaroid.imageView.frame.size.height);
    
    // Quartz 2D uses a different co-ordinate system, where the origin is in the lower left corner.
    // so, flip the Y-coordinate before we draw images on CGcontext
    CGContextTranslateCTM(ctx, 0, myPolaroid.imageView.frame.size.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    
    CGContextDrawImage(ctx, rect, myPolaroid.imageView.image.CGImage);
    CGContextSetBlendMode(ctx, kCGBlendModeNormal);
    for (UIImageView *deco in myPolaroid.decorations) {
        CGContextDrawImage(ctx, deco.frame, deco.image.CGImage);
    }
    UIImage *compositedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIImageWriteToSavedPhotosAlbum(compositedImage, self, @selector(image:hasBeenSavedWithError:usingContextInfo:), nil);
    UIGraphicsEndImageContext();
}

- (void)image:(UIImage *)image hasBeenSavedWithError:(NSError *)error usingContextInfo:(void *)ctxInfo
{
    NSLog(@"image did save");
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)addDiamondDeco
{
    NSLog(@"add diamond deco");
    Decoration *deco = [[Decoration alloc] initWithFrame:CGRectMake(100, 100, 150, 150)];
    [deco setImage:[UIImage imageNamed:@"diamonds.png"]];
    [myPolaroid addDecoration:deco];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    [popover dismissPopoverAnimated:YES];
    [myPolaroid.imageView setImage:image];
    [myPolaroid setHidden:NO];
    [self.view addSubview:myPolaroid];
    
    [decoBarButton setEnabled:YES];
}

@end
